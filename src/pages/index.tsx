import Image from "next/image";
import Navbar from "@src/components/atoms/Navbar";
import Footer from "@src/components/atoms/Footer";

export default function Home() {
  return (
    <main>
      <Navbar />
      <section className="flex flex-col gap-4 justify-center items-center mt-14">
        <div className="font-bold text-[40px]">Berrylabs Pricing</div>
        <div className="text-[#917B7D] text-2xl max-w-[684px] text-center">
          Automate Without Breaking the Bank: Affordable Solutions for Every
          Business.
        </div>
      </section>
      <section className="flex justify-center mt-[105px] mb-8">
        <div className="flex flex-col w-full max-w-[257px] gap-4 text-black text-sm">
          <div className="font-bold h-[142px] flex items-end">
            Core Features
          </div>
          <div className="h-[80px] flex items-center">Files Included</div>
          <div className="h-[24px]">Free Generations</div>
          <div className="h-[24px]">Files per Upload</div>
          <div className="h-[24px]">Pages per File</div>
          <div className="h-[24px]">File size limit</div>
          <div className="h-[24px]">High-accuracy responses</div>
          <div className="h-[24px]">Mobile-friendly interface</div>
          <div className="h-[24px]">Priority support</div>
        </div>
        <div className="flex flex-col w-full max-w-[257px] bg-[#F7FAFC] rounded-[10px] gap-4">
          <div className="flex flex-col justify-center items-center pt-7 gap-2">
            <div className="text-xl">Free</div>
            <div className="text-[13px] text-[#4F525A]">
              Suitable for Starter
            </div>
            <div className="text-[34px] text-[#08162C] font-semibold">
              IDR 0
            </div>
          </div>
          <div className="flex gap-1 justify-center bg-white items-center py-7">
            100 Files{" "}
            <Image
              src="/assets/svg/question-mark-icon.svg"
              alt="Question Mark Icon"
              className=""
              width={16}
              height={16}
              priority
            />
          </div>
          <div className="flex gap-1 h-[24px] justify-center items-center">
            100{" "}
            <Image
              src="/assets/svg/question-mark-icon.svg"
              alt="Question Mark Icon"
              className=""
              width={16}
              height={16}
              priority
            />
          </div>
          <div className="flex gap-1 h-[24px] justify-center items-center">
            Up to 2 Files
          </div>
          <div className="flex gap-1 h-[24px] justify-center items-center">
            2 pages per File{" "}
            <Image
              src="/assets/svg/question-mark-icon.svg"
              alt="Question Mark Icon"
              className=""
              width={16}
              height={16}
              priority
            />
          </div>
          <div className="flex gap-1 h-[24px] justify-center items-center">
            4MB{" "}
            <Image
              src="/assets/svg/question-mark-icon.svg"
              alt="Question Mark Icon"
              className=""
              width={16}
              height={16}
              priority
            />
          </div>
          <div className="flex gap-1 h-[24px] justify-center items-center">
            <Image
              src="/assets/svg/check-icon.svg"
              alt="Check Icon"
              className=""
              width={16}
              height={16}
              priority
            />
          </div>
          <div className="flex gap-1 h-[24px] justify-center items-center">
            <Image
              src="/assets/svg/check-icon.svg"
              alt="Check Icon"
              className=""
              width={16}
              height={16}
              priority
            />
          </div>
          <div className="flex gap-1 h-[24px] justify-center items-center">
            <Image
              src="/assets/svg/check-icon.svg"
              alt="Check Icon"
              className=""
              width={16}
              height={16}
              priority
            />
          </div>
          <div className="flex gap-1 justify-center items-center mt-12 pb-7">
            <button className="flex justify-center items-center w-full max-w-[181px] text-white text-sm font-semibold bg-[#5459D8] py-2 px-9 rounded">
              Get Started Free
            </button>
          </div>
        </div>
        <div className="flex flex-col w-full max-w-[257px] bg-white rounded-[10px] gap-4">
          <div className="flex flex-col justify-center items-center pt-7 gap-2">
            <div className="text-xl">Basic</div>
            <div className="text-[13px] text-[#4F525A]">
              Suitable for Generalist
            </div>
            <div className="text-[34px] text-[#08162C] font-semibold">
              IDR 299,000
            </div>
          </div>
          <div className="flex gap-1 justify-center bg-[#F7FAFC] items-center py-7">
            250 Files{" "}
            <Image
              src="/assets/svg/question-mark-icon.svg"
              alt="Question Mark Icon"
              className=""
              width={16}
              height={16}
              priority
            />
          </div>
          <div className="flex gap-1 h-[24px] justify-center items-center">
            100{" "}
            <Image
              src="/assets/svg/question-mark-icon.svg"
              alt="Question Mark Icon"
              className=""
              width={16}
              height={16}
              priority
            />
          </div>
          <div className="flex gap-1 h-[24px] justify-center items-center">
            Up to 50 Files
          </div>
          <div className="flex gap-1 h-[24px] justify-center items-center">
            5 pages per File{" "}
            <Image
              src="/assets/svg/question-mark-icon.svg"
              alt="Question Mark Icon"
              className=""
              width={16}
              height={16}
              priority
            />
          </div>
          <div className="flex gap-1 h-[24px] justify-center items-center">
            4MB{" "}
            <Image
              src="/assets/svg/question-mark-icon.svg"
              alt="Question Mark Icon"
              className=""
              width={16}
              height={16}
              priority
            />
          </div>
          <div className="flex gap-1 h-[24px] justify-center items-center">
            <Image
              src="/assets/svg/check-icon.svg"
              alt="Check Icon"
              className=""
              width={16}
              height={16}
              priority
            />
          </div>
          <div className="flex gap-1 h-[24px] justify-center items-center">
            <Image
              src="/assets/svg/check-icon.svg"
              alt="Check Icon"
              className=""
              width={16}
              height={16}
              priority
            />
          </div>
          <div className="flex gap-1 h-[24px] justify-center items-center">
            <Image
              src="/assets/svg/check-icon.svg"
              alt="Check Icon"
              className=""
              width={16}
              height={16}
              priority
            />
          </div>
          <div className="flex gap-1 justify-center items-center mt-12 pb-7">
            <button className="flex justify-center items-center w-full max-w-[111px] text-black text-sm py-2 px-6 border-[1px] border-[#A5A6F6] rounded">
              Select
            </button>
          </div>
        </div>
        <div className="flex flex-col w-full max-w-[257px] bg-[#F7FAFC] rounded-[10px] gap-4">
          <div className="flex flex-col justify-center items-center pt-7 gap-2">
            <div className="text-xl">Pro</div>
            <div className="text-[13px] text-[#4F525A]">
              Suitable for Specialist
            </div>
            <div className="text-[34px] text-[#08162C] font-semibold">
              IDR 590,000
            </div>
          </div>
          <div className="flex gap-1 justify-center bg-white items-center py-7">
            500 Files{" "}
            <Image
              src="/assets/svg/question-mark-icon.svg"
              alt="Question Mark Icon"
              className=""
              width={16}
              height={16}
              priority
            />
          </div>
          <div className="flex gap-1 h-[24px] justify-center items-center">
            100{" "}
            <Image
              src="/assets/svg/question-mark-icon.svg"
              alt="Question Mark Icon"
              className=""
              width={16}
              height={16}
              priority
            />
          </div>
          <div className="flex gap-1 h-[24px] justify-center items-center">
            Up to 100 Files
          </div>
          <div className="flex gap-1 h-[24px] justify-center items-center">
            10 pages per File{" "}
            <Image
              src="/assets/svg/question-mark-icon.svg"
              alt="Question Mark Icon"
              className=""
              width={16}
              height={16}
              priority
            />
          </div>
          <div className="flex gap-1 h-[24px] justify-center items-center">
            4MB{" "}
            <Image
              src="/assets/svg/question-mark-icon.svg"
              alt="Question Mark Icon"
              className=""
              width={16}
              height={16}
              priority
            />
          </div>
          <div className="flex gap-1 h-[24px] justify-center items-center">
            <Image
              src="/assets/svg/check-icon.svg"
              alt="Check Icon"
              className=""
              width={16}
              height={16}
              priority
            />
          </div>
          <div className="flex gap-1 h-[24px] justify-center items-center">
            <Image
              src="/assets/svg/check-icon.svg"
              alt="Check Icon"
              className=""
              width={16}
              height={16}
              priority
            />
          </div>
          <div className="flex gap-1 h-[24px] justify-center items-center">
            <Image
              src="/assets/svg/check-icon.svg"
              alt="Check Icon"
              className=""
              width={16}
              height={16}
              priority
            />
          </div>
          <div className="flex gap-1 justify-center items-center mt-12 pb-7">
            <button className="flex justify-center items-center w-full max-w-[111px] text-black text-sm py-2 px-6 border-[1px] border-[#A5A6F6] rounded">
              Select
            </button>
          </div>
        </div>
        <div className="flex flex-col w-full max-w-[257px] bg-white rounded-[10px] gap-4">
          <div className="flex flex-col justify-center items-center pt-7 gap-2">
            <div className="text-xl">Premium</div>
            <div className="text-[13px] text-[#4F525A]">
              Suitable for Agressive Specialist
            </div>
            <div className="text-[34px] text-[#08162C] font-semibold">
              IDR 999,000
            </div>
          </div>
          <div className="flex gap-1 justify-center bg-[#F7FAFC] items-center py-7">
            1000 Files{" "}
            <Image
              src="/assets/svg/question-mark-icon.svg"
              alt="Question Mark Icon"
              className=""
              width={16}
              height={16}
              priority
            />
          </div>
          <div className="flex gap-1 h-[24px] justify-center items-center">
            100{" "}
            <Image
              src="/assets/svg/question-mark-icon.svg"
              alt="Question Mark Icon"
              className=""
              width={16}
              height={16}
              priority
            />
          </div>
          <div className="flex gap-1 h-[24px] justify-center items-center">
            Up to 150 Files
          </div>
          <div className="flex gap-1 h-[24px] justify-center items-center">
            15 pages per File{" "}
            <Image
              src="/assets/svg/question-mark-icon.svg"
              alt="Question Mark Icon"
              className=""
              width={16}
              height={16}
              priority
            />
          </div>
          <div className="flex gap-1 h-[24px] justify-center items-center">
            16MB{" "}
            <Image
              src="/assets/svg/question-mark-icon.svg"
              alt="Question Mark Icon"
              className=""
              width={16}
              height={16}
              priority
            />
          </div>
          <div className="flex gap-1 h-[24px] justify-center items-center">
            <Image
              src="/assets/svg/check-icon.svg"
              alt="Check Icon"
              className=""
              width={16}
              height={16}
              priority
            />
          </div>
          <div className="flex gap-1 h-[24px] justify-center items-center">
            <Image
              src="/assets/svg/check-icon.svg"
              alt="Check Icon"
              className=""
              width={16}
              height={16}
              priority
            />
          </div>
          <div className="flex gap-1 h-[24px] justify-center items-center">
            <Image
              src="/assets/svg/check-icon.svg"
              alt="Check Icon"
              className=""
              width={16}
              height={16}
              priority
            />
          </div>
          <div className="flex gap-1 justify-center items-center mt-12 pb-7">
            <button className="flex justify-center items-center w-full max-w-[111px] text-black text-sm py-2 px-6 border-[1px] border-[#A5A6F6] rounded">
              Select
            </button>
          </div>
        </div>
      </section>
      <section className="flex flex-col justify-center items-center gap-8 mb-[72px]">
        <div className="font-bold text-center text-[64px] mt-[72px] max-w-[1024px]">
          Haven&apos;t found which package is right for you?
        </div>
        <div className="text-[#917B7D] text-2xl">
          We&apos;ve got you covered! Talk to our experts to find the best
          solution for you, anytime for free!
        </div>
        <button className="flex justify-center items-center w-full max-w-[221px] text-white text-2xl font-semibold bg-[#5459D8] py-4 px-9 rounded">
          Talk to expert
        </button>
      </section>
      <Footer />
    </main>
  );
}
