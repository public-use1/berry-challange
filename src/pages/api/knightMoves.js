function showPossibleMoves(pos) {
  pos = pos[1] - 1 + ("ABCDEFGH".indexOf(pos[0].toUpperCase()) << 4);

  return [-0x12, -0x21, -0x1f, -0x0e, 0x12, 0x21, 0x1f, 0x0e]
    .map((i) => pos + i)
    .filter((i) => !(i & 0x88))
    .map((i) => "ABCDEFGH"[i >> 4] + ((i & 7) + 1));
}

function knightPositions(pawnPositions) {
  let allPossiblePosition = [];
  const pawnPos = pawnPositions;
  for (element of pawnPositions) {
    const possiblePosition = showPossibleMoves(element);
    allPossiblePosition = allPossiblePosition.concat(possiblePosition);
  }
  return allPossiblePosition;
}

function findMostFrequest(arr) {
  let distribution = {};
  let max = 0;
  let result = [];

  arr.forEach(function (a) {
    distribution[a] = (distribution[a] || 0) + 1;
    if (distribution[a] > max) {
      max = distribution[a];
      result = [a];
      return;
    }
    if (distribution[a] === max) {
      result.push(a);
    }
  });
  return result;
}

console.log(findMostFrequest(knightPositions(["a1", "b6", "c3", "f8", "h4"])));
