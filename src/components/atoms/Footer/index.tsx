import Image from "next/image";

export default function Footer() {
  return (
    <footer className="flex justify-center bg-[#F9FAFB] pt-6 h-[379px]">
      <div className="flex justify-between w-[1140px]">
        <div className="flex flex-col gap-2">
          <Image
            src="/assets/svg/logo.svg"
            alt="BerryLabs Logo"
            className="cursor-pointer"
            width={272}
            height={85}
            priority
          />
          <div className="text-[#917B7D] text-sm max-w-[534px]">
            Unlocking the power of AI precision, we revolutionize the way
            businesses operate. Our innovative solution automates repetitive
            tasks, saving valuable time and boosting productivity.
          </div>
          <div className="flex mt-4 gap-4">
            <Image
              src="/assets/svg/facebook-icon.svg"
              alt="Facebook Logo"
              className="cursor-pointer"
              width={24}
              height={24}
              priority
            />
            <Image
              src="/assets/svg/twitter-icon.svg"
              alt="Twitter Logo"
              className="cursor-pointer"
              width={24}
              height={24}
              priority
            />
            <Image
              src="/assets/svg/linkedin-icon.svg"
              alt="LinkedIn Logo"
              className="cursor-pointer"
              width={24}
              height={24}
              priority
            />
            <Image
              src="/assets/svg/instagram-icon.svg"
              alt="Instagram Logo"
              className="cursor-pointer"
              width={24}
              height={24}
              priority
            />
          </div>
        </div>
        <div className="flex flex-col gap-10">
          <div className="font-bold text-xl">Pages</div>
          <div className="flex flex-col text-[#212121] text-sm gap-6">
            <div className="cursor-pointer">Home</div>
            <div className="cursor-pointer">Pricing</div>
            <div className="cursor-pointer">Solution</div>
            <div className="cursor-pointer">Demo</div>
          </div>
        </div>
        <div className="flex flex-col gap-10">
          <div className="font-bold text-xl">Service</div>
          <div className="flex flex-col text-[#212121] text-sm gap-6">
            <div className="cursor-pointer">CV Screening</div>
            <div className="cursor-pointer">Bank statement</div>
            <div className="cursor-pointer">Invoice Analyzer</div>
            <div className="cursor-pointer">Regulatory Auditor</div>
          </div>
        </div>
        <div className="flex flex-col gap-10">
          <div className="font-bold text-xl">Contact</div>
          <div className="flex flex-col text-[#212121] text-sm gap-6">
            <div className="flex gap-1 items-center cursor-pointer">
              <Image
                src="/assets/svg/phone-icon.svg"
                alt="Phone Icon"
                className=""
                width={24}
                height={24}
                priority
              />{" "}
              +6282126753060
            </div>
            <div className="flex gap-2 items-center cursor-pointer">
              <Image
                src="/assets/svg/mail-icon.svg"
                alt="Mail Icon"
                className=""
                width={24}
                height={24}
                priority
              />{" "}
              contact@berrytrada.com
            </div>
            <div className="flex gap-1 items-center cursor-pointer">
              <Image
                src="/assets/svg/location-icon.svg"
                alt="Location Icon"
                className=""
                width={24}
                height={24}
                priority
              />{" "}
              Bandung, Indonesia
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
}
