import Image from "next/image";

export default function Navbar() {
  return (
    <div className="flex justify-between items-center max-w-[1440px] m-auto py-4 px-16 h-[120px]">
      <div className="flex items-center gap-9">
        <Image
          src="/assets/svg/logo.svg"
          alt="Vercel Logo"
          className="cursor-pointer"
          width={220}
          height={87}
          priority
        />
        <div className="flex items-center py-4 px-8 text-2xl gap-2.5 font-medium cursor-pointer">
          Product{" "}
          <Image
            src="/assets/svg/arrow-icon.svg"
            alt="Vercel Logo"
            className=""
            width={14}
            height={8}
            priority
          />
        </div>
        <div className="flex items-center py-4 px-8 text-2xl gap-2.5 font-medium cursor-pointer">
          Solution{" "}
          <Image
            src="/assets/svg/arrow-icon.svg"
            alt="Vercel Logo"
            className=""
            width={14}
            height={8}
            priority
          />
        </div>
        <div className="flex items-center py-4 px-8 text-2xl gap-2.5 font-medium cursor-pointer">
          Pricing
        </div>
      </div>
      <button className="flex justify-center items-center w-full max-w-[203px] text-white text-2xl font-semibold bg-[#5459D8] py-4 px-9 rounded">
        Get Started
      </button>
    </div>
  );
}
